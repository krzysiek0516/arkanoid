package com.example.admin.gra1;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Admin on 2015-04-25.
 */
public class Pilka {

    private Bitmap bitmap; // the actual bitmap
    private int x;   // the X coordinate
    private int y;   // the Y coordinate
    private boolean touched; // if droid is touched/picked up

    public void setWait(int wait) {
        this.wait = wait;
    }

    private int wait = 0;
    public Speed getSpeed() {
        return speed;
    }

    private Speed speed;

    public Pilka(Bitmap bitmap, int x, int y) {
        this.bitmap = bitmap;
        this.speed=new Speed();
        this.x = x;
        this.y = y;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public void update() {
        if (!touched) {
            if(wait==0) {
                y += (speed.getYv() * speed.getyDirection());
                x += (speed.getXv() * speed.getxDirection());
            }
            else{
                wait--;
            }
        }
    }


    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() / 2), null);
    }

    public void handleActionDown(int eventX, int eventY) {
        if (eventX >= (x - bitmap.getWidth() / 2) && (eventX <= (x + bitmap.getWidth()/2))) {
            if (eventY >= (y - bitmap.getHeight() / 2) && (y <= (y + bitmap.getHeight() / 2))) {
                // droid touched
                setTouched(true);
            } else {
                setTouched(false);
            }
        } else {
            setTouched(false);
        }

    }
}