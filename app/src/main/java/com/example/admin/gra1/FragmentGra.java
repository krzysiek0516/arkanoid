package com.example.admin.gra1;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Admin on 2015-05-25.
 */
public class FragmentGra extends Fragment {

    View view;
    int wysokosc,szerokosc;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //pobranie wymiarow wyswietlacza
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        szerokosc = size.x/10 * 10;
        wysokosc = size.y/10*10;

        try {
            return new MainGamePanel(getActivity());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    public class MainGamePanel extends SurfaceView implements SurfaceHolder.Callback {

        private MainThread thread;
        private com.example.admin.gra1.klocek[] droid;
        private int lDroidow;
        private int zycia=3;
        Context mContext;




        private Pilka pilka = new Pilka(BitmapFactory.decodeResource(getResources(), R.drawable.klocek), 200, 3*wysokosc/10);
        private Gracz gracz = new Gracz(BitmapFactory.decodeResource(getResources(), R.drawable.gracz), 250, 8*wysokosc/10);

        private int[][] klocek = {{50,70,90,110,130,150,170,190,210,230, 250,270,290,310,330,350,370,390,410,430},
                {250, 250, 250, 250, 250, 250, 250, 250, 250, 250,250, 250, 250, 250, 250, 250, 250, 250, 250, 250}};
        private int[] zbity;
        public MainGamePanel(Context context) throws InterruptedException {
            super(context);
            this.mContext=context;
            getHolder().addCallback(this);

            lDroidow=20;
            droid = new com.example.admin.gra1.klocek[lDroidow];
            zbity = new int[lDroidow];
            for (int i = 0; i < lDroidow; i++) {
                zbity[i] = 0;
                droid[i] = new com.example.admin.gra1.klocek(BitmapFactory.decodeResource(getResources(), R.drawable.klocek), klocek[0][i], klocek[1][i]);
            }
            thread = new MainThread(getHolder(), this);
            setFocusable(true);
        }
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
        }
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            thread.setRunning(true);
            thread.start();
        }
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

            boolean retry = true;
            thread.setRunning(false);
            while (retry) {
                try {
                    thread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }
        public void update() {
            int q=0;
            for (int i = 0; i < lDroidow; i++) {
                if (zbity[i] == 1) {
                    q++;
                }
                if (q == lDroidow)
                    koniec();
            }
            if (zycia == 0) {
                koniec();
            }
            zmianaPilkaSciany();
            zmainaPilakGracz();
            zmianaPilkaDroid();

            pilka.update();
        }
        void koniec(){
            for(int i=0;i<lDroidow;i++){
                droid[i].setY(klocek[1][i]);
                droid[i].setX(klocek[0][i]);
                zbity[i]=0;
            }
            pilka.setX(250);
            pilka.setY(650);
            zycia=5;
        }
        void zmianaPilkaSciany() {

            if (pilka.getSpeed().getxDirection() == Speed.DIRECTION_RIGHT
                    && pilka.getX() + pilka.getBitmap().getWidth() / 2 >= getWidth()) {
                pilka.getSpeed().toggleXDirection();

            }
            if (pilka.getSpeed().getxDirection() == Speed.DIRECTION_LEFT
                    && pilka.getX() - pilka.getBitmap().getWidth() / 2 <= 0) {
                pilka.getSpeed().toggleXDirection();
            }
            if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
                    && pilka.getY() + pilka.getBitmap().getHeight() > gracz.getY() - gracz.getBitmap().getHeight() + 200) {
                pilka.setX(250);
                pilka.setY(500);
                pilka.setWait(500);
                pilka.getSpeed().toggleYDirection();
                zycia--;
            }
            if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_UP
                    && pilka.getY() - pilka.getBitmap().getHeight() / 2 <= 0) {
                pilka.getSpeed().toggleYDirection();
            }
        }
        void zmainaPilakGracz(){
            if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
                    && pilka.getY() + pilka.getBitmap().getHeight()/2 >= gracz.getY() - gracz.getBitmap().getHeight()/2
                    && pilka.getY() + pilka.getBitmap().getHeight()/2 <= gracz.getY() + gracz.getBitmap().getHeight()/2
                    && pilka.getX() + pilka.getBitmap().getWidth()/2  >= gracz.getX()  - gracz.getBitmap().getWidth()/2
                    && pilka.getX() - pilka.getBitmap().getWidth()/2  <= gracz.getX()  + gracz.getBitmap().getWidth()/2) {
                pilka.getSpeed().toggleYDirection();
            }

        }
        void zmianaPilkaDroid(){
            for(int i=0;i<lDroidow;i++){
                if (zbity[i] == 0) {
                    if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
                            && pilka.getY() + pilka.getBitmap().getHeight()/2 > droid[i].getY() - droid[i].getBitmap().getHeight()/2
                            && pilka.getY() + pilka.getBitmap().getHeight()/2 < droid[i].getY() + droid[i].getBitmap().getHeight()/2
                            && pilka.getX() + pilka.getBitmap().getWidth()/2  > droid[i].getX()  - droid[i].getBitmap().getWidth()/2
                            && pilka.getX() - pilka.getBitmap().getWidth()/2  < droid[i].getX()  + droid[i].getBitmap().getWidth()/2) {
                        pilka.getSpeed().toggleYDirection();
                        zbity[i]=1;
                    }
                    if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_UP
                            && pilka.getY() - pilka.getBitmap().getHeight()/2 < droid[i].getY() + droid[i].getBitmap().getHeight()/2
                            && pilka.getY() - pilka.getBitmap().getHeight()/2 > droid[i].getY() - droid[i].getBitmap().getHeight()/2
                            && pilka.getX() + pilka.getBitmap().getWidth()/2  > droid[i].getX()  - droid[i].getBitmap().getWidth()/2
                            && pilka.getX() - pilka.getBitmap().getWidth()/2  < droid[i].getX()  + droid[i].getBitmap().getWidth()/2) {
                        pilka.getSpeed().toggleYDirection();
                        zbity[i]=1;
                    }
                    if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_LEFT
                            && pilka.getX() - pilka.getBitmap().getWidth()/2 < droid[i].getX() + droid[i].getBitmap().getWidth()/2
                            && pilka.getX() - pilka.getBitmap().getWidth()/2 > droid[i].getX() - droid[i].getBitmap().getWidth()/2
                            && pilka.getY() + pilka.getBitmap().getHeight()/2  > droid[i].getY()  - droid[i].getBitmap().getHeight()/2
                            && pilka.getY() - pilka.getBitmap().getHeight()/2  < droid[i].getY()  + droid[i].getBitmap().getHeight()/2) {
                        pilka.getSpeed().toggleXDirection();
                        zbity[i]=1;
                    }
                    if (pilka.getSpeed().getyDirection() == Speed.DIRECTION_RIGHT
                            && pilka.getX() + pilka.getBitmap().getWidth()/2 > droid[i].getX() - droid[i].getBitmap().getWidth()/2
                            && pilka.getX() + pilka.getBitmap().getWidth()/2 < droid[i].getX() + droid[i].getBitmap().getWidth()/2
                            && pilka.getY() + pilka.getBitmap().getHeight()/2  > droid[i].getY()  - droid[i].getBitmap().getHeight()/2
                            && pilka.getY() - pilka.getBitmap().getHeight()/2  < droid[i].getY()  + droid[i].getBitmap().getHeight()/2) {
                        pilka.getSpeed().toggleXDirection();
                        zbity[i]=1;
                    }
                }
            }
        }

        int kolizja(){


            return 0;
        }
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                gracz.handleActionDown((int) event.getX(), (int) event.getY());
            }
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                if (gracz.isTouched()) {
                    gracz.setX((int) event.getX());
                }
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (gracz.isTouched()) {
                    gracz.setTouched(false);
                }
            }
            return true;
        }
        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(Color.BLACK);
            for (int i = 0; i < lDroidow; i++)
                if (zbity[i] == 0)
                    droid[i].draw(canvas);
            pilka.draw(canvas);
            gracz.draw(canvas);

            Paint paint = new Paint();
            paint.setColor(Color.RED);
            paint.setTextSize(20);
            canvas.drawText(Integer.toString(zycia), 20, 20, paint);
        }
    }

}
